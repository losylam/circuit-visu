/**
 * Circuit_Visu 
 * 
 * Laurent Malys pour David Rolland Chorégraphie
 *
 */

import oscP5.*;
import netP5.*;

import java.util.Map;

PFont f;
  
OscP5 osc;
PShape pattern;
int n_curve = 8;

LoRobot[] robots;
HashMap<Integer, LoRobot> robots_by_id;
int n_robots = 11;

Table table;

boolean draw_rfid;

void setup() {
  size(1480, 760);  
  frameRate(30);

  f = createFont("Arial", 32, true);
  textFont(f);
  textAlign(RIGHT);

  draw_rfid = false;

  osc = new OscP5(this, 9081);

  table = loadTable("circuit_data.csv", "header");
  pattern = loadShape("circuit.svg");

  robots = new LoRobot[n_robots];
  robots_by_id = new HashMap<Integer, LoRobot>();

  for (int i =0; i < n_robots; i++){
    TableRow row = table.getRow(i);
    robots[i] = new LoRobot(row.getInt("numero_robot"), 
                            pattern.getChild("loop"+row.getInt("numero_circuit")),
                            row.getFloat("longueur"),
                            row.getInt("n_rfid"), 
                            row.getInt("i_rfid"),
                            row.getFloat("longueur_rideau"),
                            row.getFloat("rfid_offset"));
    robots[i].set_velocity(0);
    robots_by_id.put(row.getInt("numero_robot"), robots[i]);
  }  
}

void draw() {
  background(255);
  pushMatrix();
  translate(20, 20);
  shape(pattern, 0, 0);
  for (int i =0; i < n_robots; i++){
    textAlign(RIGHT);
    robots[i].update();
    robots[i].draw_curtains();
    if (draw_rfid){
      textAlign(LEFT);
      robots[i].draw_rfid();
    }
  }
  popMatrix();
}


void keyPressed() {
  if (key == 'v'){
    draw_rfid = !draw_rfid;
  }
}

/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage theOscMessage) {
  /* print the address pattern and the typetag of the received OscMessage */
  print("### received an osc message.");
  print(" addrpattern: "+theOscMessage.addrPattern());
  println(" typetag: "+theOscMessage.typetag());
  if(theOscMessage.checkAddrPattern("/vit")==true) {
    if(theOscMessage.checkTypetag("if")) { 
      int firstValue = theOscMessage.get(0).intValue();  
      float secondValue = theOscMessage.get(1).floatValue();
      robots_by_id.get(firstValue).set_velocity(secondValue/1000);
      print("### received an osc message /vit with typetag if.");
      println(" values: "+firstValue+", "+secondValue);
    }
  }else if(theOscMessage.checkAddrPattern("/moteur")==true) {
    if(theOscMessage.checkTypetag("iii")) { 
      int firstValue = theOscMessage.get(0).intValue();  
      int secondValue = theOscMessage.get(1).intValue();
      int rampe = theOscMessage.get(2).intValue();
      float vel = secondValue/1000.0;
      if (rampe <0){
         vel = -vel; 
      }
      robots_by_id.get(firstValue).set_velocity_m(vel);
      print("### received an osc message /vit with typetag if.");
      println(" values: "+firstValue+", "+vel);
    }
  }else if(theOscMessage.checkAddrPattern("/pos_abs")==true) {
    if(theOscMessage.checkTypetag("if")) { 
      int firstValue = theOscMessage.get(0).intValue();  
      float secondValue = theOscMessage.get(1).floatValue();
      robots_by_id.get(firstValue).set_position(secondValue);
      print("### received an osc message /pos with typetag if.");
      println(" values: "+firstValue+", "+secondValue);
    }
  }else if(theOscMessage.checkAddrPattern("/pos")==true) {
    if(theOscMessage.checkTypetag("ii")) { 
      int firstValue = theOscMessage.get(0).intValue();  
      int secondValue = theOscMessage.get(1).intValue();
      robots_by_id.get(firstValue).set_position_rfid(secondValue-1);
      print("### received an osc message /pos with typetag if.");
      println(" values: "+firstValue+", "+secondValue);
    }
  }else if (theOscMessage.checkAddrPattern("/goto")==true){
    if(theOscMessage.checkTypetag("iifi")) {
      int i_curve = theOscMessage.get(0).intValue(); 
      int i_rfid = theOscMessage.get(1).intValue();  
      float t = theOscMessage.get(2).floatValue();
      int rampe = theOscMessage.get(3).intValue();
      robots_by_id.get(i_curve).goto_rfid(i_rfid, t, rampe);
      println(" values: " + t);
    }
   }else if (theOscMessage.checkAddrPattern("/aller")==true){
    if(theOscMessage.checkTypetag("iiii")) {
      int i_curve = theOscMessage.get(0).intValue(); 
      int i_rfid = theOscMessage.get(1).intValue();  
      int v = theOscMessage.get(2).intValue();
      float vel = v/1000.0;
      int rampe = theOscMessage.get(3).intValue();
      robots_by_id.get(i_curve).aller_rfid(i_rfid, vel, rampe);
      //println(" values: "+10+", "+parseInt("23"));
    }
   }else if (theOscMessage.checkAddrPattern("/gototest")==true){
      if(theOscMessage.checkTypetag("iif")) {
          int i_robot = theOscMessage.get(0).intValue();
          int i_rfid = theOscMessage.get(1).intValue();
          float t = theOscMessage.get(2).floatValue();
          robots_by_id.get(i_robot).goto_rfid(i_rfid, t, 0);
          print("### received an osc message gototest");
          println(" values: "+i_robot+", "+i_rfid); 
    }
  }else if (theOscMessage.checkAddrPattern("/init")==true){
    for (int i = 0; i<n_robots;i++){
      robots[i].set_position(0);
      robots[i].set_velocity(0); 
    }
  }else if (theOscMessage.checkAddrPattern("/stop")==true){
     int i_robot = theOscMessage.get(0).intValue();
     robots_by_id.get(i_robot).set_velocity(0);
     println(" values: "+i_robot);
  }
}
