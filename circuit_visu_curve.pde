class GotoItem {
   public int rfid;
   public int t;
   
   GotoItem(int rfid_in, int t_in){
     rfid = rfid_in;
     t = t_in;
   }
}

class LoRobot {
  private int numero_robot;

  private float curveLength;
  private float curve_length_m;
  private float pix_per_m;
  private PShape shape;
  private PVector[] lst_points;
  private float[] dists;
  private float[] pos;
  private int n_pas = 100;
  private int n_seg;
  
  private int n_rfid;
  private float[] rfid;
  private int[] rfid_pos_pt;
  private float rfid_offset;
  private boolean ongoing;
  private float goto_end;
  
  private ArrayList<GotoItem> goto_items;
  
  // handle velocity
  private float velocity;
  private PVector current_pos_pt;
  private float current_pos_abs;
  private int current_pos_i;
  private int old_millis;
  
  // curtain
  private float curtain_length;
  
  LoRobot(int numero_robot_in, 
          PShape shape_in, 
          float curve_length_m_in, 
          int n_rfid_in, 
          int i_rfid_in,
          float curtain_length_in, 
          float rfid_offset_in){
    numero_robot = numero_robot_in;
    shape = shape_in;
    int total = shape.getVertexCount();
    curve_length_m = curve_length_m_in;
    n_rfid = n_rfid_in;
    rfid_offset = rfid_offset_in;
    n_seg = total;
    velocity = 0.0;
    ongoing = false;
    goto_end = 0;
    goto_items = new ArrayList();
    lst_points = new PVector[total];
    lst_points[0] = shape.getVertex(0);
    for (int i = 1; i < total; i++){
      PVector p = shape.getVertex(total - i);
      //PVector p = shape.getVertex(i);
      lst_points[i] = p;
    }

    measure();

    current_pos_abs = rfid[i_rfid_in];
    
    current_pos_i = rfid_pos_pt[i_rfid_in];
    current_pos_pt = lst_points[current_pos_i];

    curtain_length = curtain_length_in * pix_per_m / 2;

    println(curveLength, curve_length_m, pix_per_m, curtain_length, curtain_length_in);
  }
  
  void measure(){
    dists = new float[n_seg];
    pos = new float[n_seg];
    for (int i = 1; i<n_seg; i++){
      dists[i-1] = dist(lst_points[i-1].x,lst_points[i-1].y, lst_points[i].x,lst_points[i].y);
      curveLength += dists[i-1];
      pos[i-1] = curveLength;
    }
    dists[n_seg-1] = dist(lst_points[0].x,lst_points[0].y, lst_points[n_seg-1].x,lst_points[n_seg-1].y);
    curveLength += dists[n_seg-1];
    pos[n_seg-1] = curveLength;
    
    rfid = new float[n_rfid];
    rfid_pos_pt = new int[n_rfid];
    for (int i = 0; i<n_rfid;i++){
      float rfid_pos = (rfid_offset * curveLength) + i * curveLength / n_rfid;
      while (rfid_pos >= curveLength){
        rfid_pos = rfid_pos - curveLength;
      }
      rfid[i] = rfid_pos;
      //println(rfid[i]);
    }
    pix_per_m = curveLength / curve_length_m;
    println(pix_per_m);
 
    for (int i = 0; i<n_rfid;i++){
      rfid_pos_pt[i] = get_point_at_pos_i(rfid[i]);
    }
    current_pos_abs = rfid[0];
  }
  
  void set_velocity(float velocity_in){
    velocity = velocity_in;
    //old_millis = millis();
  }

  void set_velocity_m(float velocity_in){
    set_velocity(velocity_in*pix_per_m/1000.0);
    println(" values: "+(velocity_in*pix_per_m));
    //old_millis = millis();
  }
  
  void set_position(float dis){
    if ((dis >=0) && (dis <1)){
      current_pos_abs = curveLength * dis; 
    }
  }
  
  void set_position_rfid(int i_rfid){
    if (i_rfid < n_rfid){
      current_pos_abs = rfid[i_rfid]; 
    }
  }
  

  int get_point_at_pos_i(float pos_in){
    float cur_pos = 0;
    int cur_pt = 0;
    while (cur_pos < pos_in){
      cur_pt += 1;
      cur_pos = pos[cur_pt];
    }
    return cur_pt;
  }
  
  
   float get_dist_rfid(int i_rfid, int rampe){
     float d =0;
        if (current_pos_abs < rfid[i_rfid]){
          d = rfid[i_rfid] - current_pos_abs;
        }else{
          d = (curveLength - current_pos_abs) + rfid[i_rfid];      
        }
        if (rampe <0){
          d = -(curveLength - d); 
        }
      return d; 
   }
  void aller_rfid(int i_rfid, float v, int rampe){
    if ((i_rfid < n_rfid) && (!ongoing)){
      float d = get_dist_rfid(i_rfid, rampe);
      v = v*pix_per_m/1000.0;
      float t = d/v;
      goto_end = millis() + t;
      ongoing = true;
      set_velocity(v);
       println("get goto current : "+current_pos_abs+", rfid pos "+rfid[i_rfid]+", d : " + d);  
       println("set goto : "+i_rfid+" in "+t+" ms at vel : " + v);  
    }
  }
  
  void goto_rfid(int i_rfid, float t_in, int rampe){
    int t = int(t_in*1000);
    if (i_rfid < n_rfid){
      if (ongoing){
        goto_items.add(new GotoItem(i_rfid, t));
      }else{
        goto_end = millis() + t;
        ongoing = true;
        
        float d = get_dist_rfid(i_rfid, rampe);
        
       float new_vel = d/t;
       set_velocity(new_vel);
    
       println("get goto current : "+current_pos_abs+", rfid pos "+rfid[i_rfid]+", d : " + d);  
       println("set goto : "+i_rfid+" in "+t+" ms at vel : " + new_vel);  
      }
    }
  }
  
  PVector get_new_pos(){
    int now = millis();
    int dt = now - old_millis;
    old_millis = now;
    
    current_pos_abs = current_pos_abs + dt * velocity;
    if (current_pos_abs > curveLength){
      current_pos_abs = current_pos_abs - curveLength;
    }else if (current_pos_abs < 0){
       current_pos_abs = curveLength + current_pos_abs;
    }
    current_pos_i = get_point_at_pos_i(current_pos_abs);
    current_pos_pt = lst_points[current_pos_i];

    return current_pos_pt; 
  }
  
  void update(){
     PVector cur_pos = get_new_pos();
     if (ongoing){
        if (goto_end < millis()){
            if (goto_items.size() == 0){
            set_velocity(0);
            ongoing = false;
            goto_end = 0.0;
        }else{
          ongoing = false;
          goto_rfid(goto_items.get(0).rfid, goto_items.get(0).t, 0);
          goto_items.remove(0);
        }
        }
     }
  }
  
  void draw_curtains(){

    float curtain_queue_pos = current_pos_abs - curtain_length;
    float curtain_head_pos = current_pos_abs + curtain_length;
    int curtain_queue_pos_i;
    int curtain_head_pos_i;
    //println("queue_pos "+ curtain_queue_pos);
    
    if (curtain_queue_pos < 0){
      curtain_queue_pos = curveLength + curtain_queue_pos;
    }
    if (curtain_head_pos > curveLength){
      curtain_head_pos = curtain_head_pos - curveLength;
    }
    
    // draw rfid pos
    fill(0, 0, 0);
    stroke(0, 0, 0);
    for (int i = 0; i<n_rfid;i++){
      ellipse(lst_points[rfid_pos_pt[i]].x,lst_points[rfid_pos_pt[i]].y, 2, 2);
    }
    
    curtain_queue_pos_i = get_point_at_pos_i(curtain_queue_pos);
    curtain_head_pos_i = get_point_at_pos_i(curtain_head_pos);

    noFill();
    beginShape();
    stroke(250, 0, 0);
    strokeWeight(8);

    int pt0 = curtain_queue_pos_i;
    
    while ((pt0 > curtain_head_pos_i)&&(pt0 < n_seg)){
      vertex(lst_points[pt0].x, lst_points[pt0].y);
      pt0+=1;
    }
    if (pt0>=n_seg){
      pt0 = 0; 
    }
    while (pt0 < curtain_head_pos_i){
      vertex(lst_points[pt0].x, lst_points[pt0].y);
      pt0+=1;
    }

    endShape();
    ellipse(lst_points[current_pos_i].x,lst_points[current_pos_i].y, 6, 6);
    fill(200, 0, 0);
     textFont(f, 30);
    text(numero_robot, lst_points[current_pos_i].x-10, lst_points[current_pos_i].y+20);
  }

  void draw_rfid(){
    for (int i = 0; i<n_rfid;i++){
      textFont(f, 20);
      stroke(0,200,0);
      fill(0,200,0);
      text(str(i+1), lst_points[rfid_pos_pt[i]].x+5,lst_points[rfid_pos_pt[i]].y-5);
      fill(0,200,0);
      ellipse(lst_points[rfid_pos_pt[i]].x,lst_points[rfid_pos_pt[i]].y, 2, 2);
    }
  }

};
